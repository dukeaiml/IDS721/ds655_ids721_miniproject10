# Mini Project 10

## Rust Serverless Transformer Endpoint

##  Requirements
-   Dockerize Hugging Face Rust transformer
-   Deploy container to AWS Lambda
-   Implement query endpoint

##  Grading Criteria
-   Transformer packaging: 30%
-   Serverless deployment: 30%
-   Endpoint functionality: 30%
-   Documentation: 10%

##  Deliverables
-   Dockerfile and Rust code
-   Screenshot of AWS Lambda
-   cURL request against endpoint
